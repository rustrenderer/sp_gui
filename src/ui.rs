use imgui::Context;

use glutin::event::{ElementState, Event, ModifiersState, MouseButton, WindowEvent};
use glutin::window::Window;

pub struct Ui {
    pub(crate) context: Context,
}

impl Ui {
    pub fn new() -> Self {
        let mut context = imgui::Context::create();

        context.io_mut().want_capture_keyboard = true;
        context.io_mut().want_capture_mouse = true;
        context.io_mut().nav_active = true;
        context.io_mut().nav_visible = true;
        context.io_mut().mouse_draw_cursor = false;

        Self { context }
    }

    pub fn update(&mut self, window: &Window) {
        let size = window.inner_size();
        self.context.style_mut().mouse_cursor_scale = window.scale_factor() as f32;
        self.set_display_size(size.width as f32, size.height as f32);
    }

    fn dispatch_window_event(&mut self, event: &glutin::event::WindowEvent) {
        match event {
            WindowEvent::Resized(size) => {
                self.set_display_size(size.width as f32, size.height as f32)
            }
            WindowEvent::ReceivedCharacter(_) => {}
            WindowEvent::Focused(_) => {}
            WindowEvent::KeyboardInput {
                device_id: _,
                input,
                is_synthetic: _,
            } => {
                self.set_key_down(input.scancode, &input.state);
            }
            WindowEvent::ModifiersChanged(modifiers) => {
                self.set_modifier(modifiers);
            }
            WindowEvent::CursorMoved {
                device_id: _,
                position,
                ..
            } => {
                let position =
                    position.to_logical::<f64>(self.context.style().mouse_cursor_scale as f64);
                self.set_cursor_position(position.x as f32, position.y as f32);
            }
            WindowEvent::CursorEntered { .. } => {}
            WindowEvent::CursorLeft { .. } => {}
            WindowEvent::MouseWheel { .. } => {}
            WindowEvent::MouseInput {
                device_id: _,
                state,
                button,
                ..
            } => {
                self.set_mouse_down(button, state);
            }
            WindowEvent::TouchpadPressure { .. } => {}
            WindowEvent::AxisMotion { .. } => {}
            WindowEvent::Touch(_) => {}
            WindowEvent::ScaleFactorChanged { .. } => {}
            WindowEvent::ThemeChanged(_) => {}
            _ => {}
        }
    }

    pub fn dispatch_event<T>(&mut self, in_event: &glutin::event::Event<T>) {
        match in_event {
            Event::WindowEvent {
                window_id: _,
                event,
            } => {
                self.dispatch_window_event(event);
            }
            //Event::DeviceEvent {device_id, event} => {
            //    self.dispatch_device_event(event);
            //},
            _ => {}
        }
    }

    pub fn set_display_size(&mut self, width: f32, height: f32) {
        self.context.io_mut().display_size[0] = width;
        self.context.io_mut().display_size[1] = height;
    }

    pub fn set_cursor_position(&mut self, x: f32, y: f32) {
        self.context.io_mut().mouse_pos[0] = x;
        self.context.io_mut().mouse_pos[1] = y;
    }

    fn set_modifier(&mut self, modifiers: &ModifiersState) {
        self.context.io_mut().key_alt = modifiers.ctrl();
        self.context.io_mut().key_ctrl = modifiers.alt();
        self.context.io_mut().key_shift = modifiers.shift();
        self.context.io_mut().key_super = modifiers.logo();
    }

    fn set_mouse_down(&mut self, button: &MouseButton, state: &ElementState) {
        let button_id: usize = match button {
            MouseButton::Left => 0,
            MouseButton::Right => 1,
            MouseButton::Middle => 2,
            MouseButton::Other(v) => *v as usize,
        }
        .min(4);

        self.context.io_mut().mouse_down[button_id] = match state {
            ElementState::Pressed => true,
            ElementState::Released => false,
        };
    }

    fn set_key_down(&mut self, key: u32, state: &ElementState) {
        self.context.io_mut().keys_down[key as usize] = match state {
            ElementState::Pressed => true,
            ElementState::Released => false,
        };
    }

    pub fn set_alpha(&mut self, alpha: f32) {
        self.context.style_mut().alpha = alpha;
    }

    pub fn frame(&mut self) -> imgui::Ui {
        self.context.frame()
    }

    pub fn is_ctrl_down(&self) -> bool {
        self.context.io().key_ctrl
    }

    pub fn is_alt_down(&self) -> bool {
        self.context.io().key_alt
    }

    pub fn is_shift_down(&self) -> bool {
        self.context.io().key_shift
    }

    pub fn is_super_down(&self) -> bool {
        self.context.io().key_super
    }
}
