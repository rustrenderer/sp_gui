
use imgui_opengl_renderer::Renderer as _Renderer;
use glutin::PossiblyCurrent;

pub struct Renderer {
    internal: _Renderer,
}

impl Renderer {
    pub fn new (ui: &mut super::ui::Ui, gl_context: &glutin::Context<PossiblyCurrent>) -> Self {
        let internal = _Renderer::new(
            &mut ui.context,
            |s| gl_context.get_proc_address(s) as _
        );

        Self {
            internal,
        }
    }

    pub fn update (&mut self) {

    }

    pub fn render (&self, ui : imgui::Ui) {
        self.internal.render(ui);
    }
}