
pub extern crate glutin;
pub extern crate imgui;
pub extern crate imgui_opengl_renderer;

pub mod ui;
pub mod renderer;

use glutin::PossiblyCurrent;

pub fn init (gl_context: &glutin::Context<PossiblyCurrent>) -> (ui::Ui, renderer::Renderer) {
    let mut gui = ui::Ui::new();
    let renderer = renderer::Renderer::new(
        &mut gui,
        gl_context
    );

    (gui,renderer)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
